package Bin_Packing;

import java.io.*;
import java.util.stream.Collectors;
import java.util.Scanner;
import java.util.ArrayList;
import java.lang.*;
import java.util.PriorityQueue;
import java.util.Comparator;
import java.util.Collections;
import java.util.Arrays;


public class NFD extends NF
{


  public static void main(String[] args)
  {
    File dataFile = null;
    //get file from comman line
    if(0 < args.length )
    {
      dataFile = new File(args[0]);

      ArrayList<Service> service_list = new ArrayList<Service>();
      ArrayList<Machine> machine_list = new ArrayList<Machine>();
      //new object for NF
      NF data = new NF();

      service_list = data.ReadFile(dataFile);
      machine_list = data.create_machine_list();
      long start = System.currentTimeMillis();
      service_list = data.count_sort(service_list);
      machine_list = data.next_fit(service_list,machine_list);
      long end = System.currentTimeMillis();

      //print out the result of the experiment
      int solution = data.acc_solution(machine_list);
      System.out.println(end-start+"ms");
      System.out.println("LB solution: " + data.optimal_solution(service_list));
      System.out.println("actual solution: " + solution);
      System.out.println("Objective function in % (actual - LB/LB)*100 : " + (solution-data.optimal_solution(service_list))/(double)data.optimal_solution(service_list) * 100);

    }
    else
    {
      System.err.println("Invalid arguments count:" + args.length);
      System.exit(0);
    }
  }

  /*
    //BULK run for the EXPERIMENT for each data set sizes
    public static void main(String[] args)
    {
      //variable for data
      long[] time_list = new long[100];
      int[] op_list = new int[100];
      int[] acc_list = new int[100];
      long avg_time = 0;

      File dataFile = null;
      //get file from command line
      if(0 < args.length && !args[0].equals("full"))
      {
        int[] list_op = new int[100];
        int avg_op = 0;

        int[] list_acc = new int[100];
        double avg_acc = 0;
        System.out.println("dataset_"+args[0]+": ");
        for(int i = 0; i < 100 ; i++)
        {
          dataFile = new File("/Users/ed_garden/Documents/Individual_projects/heuristic/dataset1/apps_"+args[0]+"/dataset_"+args[0]+"_"+i+".csv");
          //new object for NFD
          NF data = new NF();

          ArrayList<Service> service_list = new ArrayList<Service>();
          ArrayList<Machine> machine_list = new ArrayList<Machine>();

          service_list = data.ReadFile(dataFile);
          machine_list = data.create_machine_list();

          long start = System.currentTimeMillis();
          service_list = data.count_sort(service_list);
          machine_list = data.next_fit(service_list,machine_list);
          long end = System.currentTimeMillis();

          // analaysis
          time_list[i] = end-start;
          op_list[i] = data.optimal_solution(service_list);
          acc_list[i] = data.acc_solution(machine_list);
        }

        //print out analaysis and avg analaysis
        NF data = new NF();

        System.out.println("Duration: "+Arrays.toString(time_list));
        System.out.println("LB Executor: "+Arrays.toString(op_list));
        System.out.println("Actual Executor: "+Arrays.toString(acc_list));

        for(int i = 0; i < 100; i++)
        {
          avg_time += time_list[i];
        }
        System.out.println("Average time taken is: "+ avg_time/100);
        double[] grade = data.grade_heuristic(op_list,acc_list);
        System.out.println("Accuracy in % (actual - LB/LB)*100 : " + Arrays.toString(grade));
        System.out.println("Final Accuracy(%): "+ data.avg_grade(grade)+ "%");
      }
      else
      {
        System.err.println("Invalid arguments count:" + args.length);
        System.exit(0);
      }
    }*/
}
