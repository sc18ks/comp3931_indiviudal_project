//import junit.framework.TestCase;
package Test;
import Bin_Packing.*;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;


public class ServiceTest
{
  private Service s1;
  private Service s2;
  private Service s3;

  @Before
  public void setUp() throws Exception
  {
      s1 = new Service(1,10,"[(2, 0)]");
      s2 = new Service(2,10,"[(3, 1)]");
      s3 = new Service(3,10,"[]");
  }

  @Test //test variable in executors
  public void testVarService()
  {
    assertEquals("service id should be 1", 1, s1.service_id);
    assertEquals("should have 1 replica", 0, s1.nb_instances);
    assertEquals("should have 10 core", 10, s1.core);

  }

  @Test //test getter method
  public void testGetAffService()
  {
    assertEquals("service conflict list size should be 2", 2, s1.getAff_listSize());
    assertEquals("service conflict list size should be 2", 2, s2.getAff_listSize());
    assertEquals("service conflict list size should be 1", 1, s3.getAff_listSize());
  }

  @Test //get size
  public void testCheckAffService()
  {
    // if it have conflict return false
    assertEquals("have conflict with service 2", false, s1.check_aff(2));
    assertEquals("have no conlict with service 3", true, s1.check_aff(3));
    assertEquals("have conlict with service 3", false, s2.check_aff(3));

  }






}
