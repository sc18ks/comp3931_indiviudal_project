//import junit.framework.TestCase;
package Test;
import Bin_Packing.*;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.stream.Collectors;
import java.util.Scanner;
import java.util.ArrayList;
import java.lang.*;
import java.util.PriorityQueue;
import java.util.Comparator;
import java.util.Collections;
import java.util.Arrays;



public class InstanceTestLarge
{
  private File list = null;


  //FF
  private ArrayList<Service> service_list;
  private ArrayList<Machine> machine_list;
  private int LB_solution_FF;
  private int Actual_solution_FF;

  //NF
  private ArrayList<Service> service_NF;
  private ArrayList<Machine> machine_NF;
  private int LB_solution_NF;
  private int Actual_solution_NF;

  //BF
  private ArrayList<Service> service_BF;
  private ArrayList<Machine> machine_BF;
  private int LB_solution_BF;
  private int Actual_solution_BF;

  //WF
  private ArrayList<Service> service_WF;
  private ArrayList<Machine> machine_WF;
  private int LB_solution_WF;
  private int Actual_solution_WF;
  @Before
  public void setUp() throws Exception
  {
      File list = new File("/Users/ed_garden/Documents/Individual_projects/heuristic/dataset1/test_instances/test2.csv");
      ArrayList<Service> service_list = new ArrayList<Service>();

      //For FF
      FF data = new FF();
      service_list = data.ReadFile(list);


      ArrayList<Machine> machine_list = new ArrayList<Machine>();
      machine_list = data.create_machine_test_List_L();
      machine_list = data.first_fit(service_list,machine_list);
      LB_solution_FF = data.optimal_solution(service_list);
      Actual_solution_FF = data.acc_solution(machine_list);

      //For NF
      NF dataNF = new NF();
      service_NF = dataNF.ReadFile(list);

      ArrayList<Machine> machine_NF = new ArrayList<Machine>();
      machine_NF = dataNF.create_machine_test_List_L();
      machine_NF = dataNF.next_fit(service_NF,machine_NF);
      LB_solution_NF = dataNF.optimal_solution(service_NF);
      Actual_solution_NF = dataNF.acc_solution(machine_NF);

      //For BF
      BF dataBF = new BF();
      service_BF = dataBF.ReadFile(list);

      Comparator<Machine> coreSorter = Comparator.comparing(Machine::getCore);
      PriorityQueue<Machine> machine_BF = new PriorityQueue<Machine>(coreSorter);
      machine_BF = dataBF.create_machine_test_L();
      machine_BF = dataBF.best_fit(service_BF,machine_BF);
      LB_solution_BF = dataBF.optimal_solution(service_BF);
      Actual_solution_BF = dataBF.heuristic_solution(machine_BF);

      //For WF
      WF dataWF = new WF();
      service_WF = dataWF.ReadFile(list);

      Comparator<Machine> coreSorter1 = Comparator.comparing(Machine::getCore).reversed();
      PriorityQueue<Machine> machine_WF = new PriorityQueue<Machine>(coreSorter1);
      machine_WF = dataWF.create_machine_WF_L(machine_WF);
      machine_WF = dataWF.worst_fit(service_WF,machine_WF);
      LB_solution_WF = dataWF.optimal_solution(service_WF);
      Actual_solution_WF = dataWF.heuristic_solution(machine_WF);
  }

  @Test //test variable in executors
  public void testObjFF()
  {
    assertEquals("Lower bound should be 424", 424, LB_solution_FF);
    assertEquals("Actual solution for FF should be 12995",12998, Actual_solution_FF);
  }

  @Test //test variable in executors
  public void testObjNF()
  {
    assertEquals("Lower bound should be 424", 424, LB_solution_NF);
    assertEquals("Actual solution for NF should be 12998",12998, Actual_solution_NF);
  }

  @Test //test variable in executors
  public void testObjBF()
  {
    assertEquals("Lower bound should be 424", 424, LB_solution_BF);
    assertEquals("Actual solution for BF should be 12995",12998, Actual_solution_BF);
  }

  @Test //test variable in executors
  public void testObjWF()
  {
    assertEquals("Lower bound should be 424", 424, LB_solution_WF);
    assertEquals("Actual solution for WF should be 13000",13000, Actual_solution_WF);
  }




}
