package Bin_Packing;

import java.util.ArrayList;
import java.util.*;

public class Service
{
  public int service_id;
  public int nb_instances;
  public int core;
  public Set<Integer> aff_list =new HashSet<Integer>();


  public Service(int service_id, int core, String inter_aff)
  {
    this.service_id = service_id;
    this.core = core;
    aff_list.add(service_id);   //service always have affinity with itself
    FillLists(inter_aff);       // fill the inter_aff list
  }

  // use this string to fill in aff_list
  // use this string to fill in interaff_servie_amount
  public void FillLists(String inter_list)
  {
    //extrating inter_aff
    String[] inter_parts = inter_list.trim().split("\\(|\\)");
    int count = 0;
    String[] value = null;
    while(inter_parts.length > count)
    {
      //print out odd value
      if(count%2 != 0)
      {
        value = inter_parts[count].trim().split(",");
        aff_list.add(Integer.parseInt(value[0]));        // add amount of service that it will tolerate
      }
      count ++;
     }
    }

  public int getAff_listSize()
  {
    return aff_list.size();
  }

  //return false if this service does not have conflict
  public boolean check_aff(int aff)
  {
    if(this.aff_list.isEmpty())
    {
      return true;
    }
    
    if(this.aff_list.contains(aff))
    {
      return false;
    }
    return true;
  }

}
