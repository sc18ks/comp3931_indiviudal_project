Application description
=======================

The description of all applications is in the `application_tclab-0.csv` CSV file (columns are separated by `\t`).

Each row describes one application with the following columns:

- app_id: The ID of the application
- nb_instances: The number of identical instances (replicas) of the application
- core: The core requirement of *each* instance
- memory: The memory requirement of *each* instance
- disk: The disk requirement of *each* instance
- intra_value: The intra-affinity value for this app
- inter_degree: The number of *outer* edges in the conflict graph for this app
- inter_aff: The list of tuples (app_id, k) corresponding to the outer edges for this app. These are the inter-affinity relations.



Machine description
===================

The machines of the `alibaba-tclab-0` dataset is originally composed of 3,000 identical machines of type 1 + 3,000 identical machines of type 2.
For the purpose of our experiments, we can consider the following machine sets (coming from both tclab-0 and tclab-1 traces):

- Set1: 3,000 machines with for each 32 cores, 64 units of memory, 1440 units of disk.
- Set2: 6,000 machines with for each 32 cores, 64 units of memory, 1440 units of disk.
- Set3: 2,000 machines with for each 92 cores, 288 units of memory, 2457 units of disk.
- Set4: 3,000 machines with for each 92 cores, 288 units of memory, 2457 units of disk.
- Set5: 8,000 machines with for each 92 cores, 288 units of memory, 2457 units of disk.

