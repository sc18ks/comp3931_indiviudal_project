//import junit.framework.TestCase;
package Test;
import Bin_Packing.*;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;


public class BPTest
{
  private Machine m1;
  private Machine m2;
  private Machine m3;

  private Service s1;
  private Service s2;
  private Service s3;

  @Before
  public void setUp() throws Exception
  {
      //PriorityQueue<Machine> mac_l = new PriorityQueue<Machine>();

      m1 = new Machine(1,10);
      m2 = new Machine(2,10);
      m3 = new Machine(3,10);

      s1 = new Service(0,0,"[]");
      s2 = new Service(0,0,"[]");
      s3 = new Service(0,0,"[]");
  }

  @Test //test variable in executors
  public void testVarMachine()
  {
    assertEquals("machine id should be 1", 1, m1.machine_id);
    assertEquals("residual_core_size should be 10",10, m1.residual_core_size);
  }


  @Test //test getter method
  public void testGetMachine()
  {
    assertEquals("machine size should be 10", 10, m1.getCore());
    assertEquals("conflict size should be zero", 0, m1.getAff_listSize());
  }

  @Test //test if compare to method work for queue
  public void testCompareMachine()
  {
    assertEquals("should return 0",m1.compareTo(m2),0);
  }

  @Test //test add service method
  public void testAddServiceMachine()
  {
    m2.add_service(2,3);
    m2.add_service(3,3);
    assertEquals("machine should now have 4 core", 4, m2.getCore());
  }

  @Test //test for boolean if the residual capacity checker is right
  public void testCheckEnoughCoreMachine()
  {
    assertEquals("should return true 10 = 10", true, m1.residual_capacity(10));
    assertEquals("should return true 9 < 10", true, m1.residual_capacity(9));
    assertEquals("should return false 11 > 10", false, m1.residual_capacity(11));
  }

  @Test //test for boolean if the conflict checker is right
  public void testCheckAffMachine()
  {
    assertEquals("should return true 10 = 10", true, m1.check_affinity(10));
    assertEquals("should return true 9 < 10", true, m1.check_affinity(9));
    assertEquals("should return false 11 > 10", true, m1.check_affinity(11));
  }




}
