package Bin_Packing;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.*;
import java.io.*;

public class Machine implements Comparable<Machine>
{
  public int machine_id ;
  public int residual_core_size;
  public Set<Integer> aff_list =new HashSet<Integer>();               // service aff that stay is in this machine
  public ArrayList<Integer> service_list = new ArrayList<Integer>();  // have this becuase cant have 2 same service in same set


  public Machine(int machine_id, int residual_core_size)
  {
    this.machine_id = machine_id ;
    this.residual_core_size = residual_core_size;
  }

  //return true if the machine have enough capacity for this service
  public boolean residual_capacity(int service_core_size)
  {
    if(this.residual_core_size >= service_core_size)
    {
      return true;
    }
    else return false;
  }



  // return true if this machine does not have conflict with this aff
  public boolean check_affinity(int aff)
  {
    //if the list is null
    if(this.aff_list.isEmpty())
    {
      return true;
    }

    if(this.aff_list.contains(aff))
    {
      return false;
    }
    return true;
  }



  //accept this service to machine and update residual_capacity
  public void add_service(int service_id, int service_core_size)
  {
    this.service_list.add(service_id);
    this.residual_core_size = this.residual_core_size - service_core_size;
  }

  //add all conflict in that service to this machine
  public void add_aff(Set<Integer> service_aff)
  {
    this.aff_list.addAll(service_aff);
  }

  @Override
  public int compareTo(Machine mac)
  {
    return this.getCore() - mac.getCore();
  }


  //getter and setters
  public int getCore()
  {
    return this.residual_core_size;
  }

  public int getAff_listSize()
  {
    return aff_list.size();
  }

  public void setCore(int service_core)
  {
    this.residual_core_size -= service_core;
  }

  @Override
  public String toString()
  {
    return "id: " + this.machine_id +" core: "+ this.residual_core_size + " service: " + service_list + " aff list: " + aff_list + "\n";
  }


}
