package Bin_Packing;

import java.io.*;
import java.util.stream.Collectors;
import java.util.Scanner;
import java.util.ArrayList;
import java.lang.*;
import java.util.PriorityQueue;
import java.util.Comparator;
import java.util.Collections;
import java.util.Arrays;


public class NF extends Heuristic
{
  //next Fit
  public static ArrayList<Machine> next_fit(ArrayList<Service> service_list, ArrayList<Machine> machine_list)
  {
    ArrayList<Machine> closed_machine_list = new ArrayList<Machine>();
    boolean checker_mac_isEmpty = false;
    boolean checker_mac = false;
    for(int i = 0; i < service_list.size();i++)
    {
      for(int j = 0; j < machine_list.size(); j++)
      {
        checker_mac_isEmpty = false;
        checker_mac = false;

        //return true if machine affinity list is empty, wont go in the machine list loop
        if(machine_list.get(j).aff_list.isEmpty())
        {
          checker_mac_isEmpty = true;
        }
        //return true service list aff does not have conflict with machine aff
        //if any of them is false then no allocation
        for (int aff : service_list.get(i).aff_list)
        {
          if(machine_list.get(j).check_affinity(aff))
          {
            checker_mac = true;
          }
          else
          {
            checker_mac = false;
            break;
          }
        }

        //add the service and its affinity in the machine
        if((checker_mac_isEmpty == true || checker_mac == true) && machine_list.get(j).residual_capacity(service_list.get(i).core))
        {
          machine_list.get(j).add_service(service_list.get(i).service_id,service_list.get(i).core);
          machine_list.get(j).add_aff(service_list.get(i).aff_list);
          break; //if finsh allocating into the machine
        }
        else
        {
          closed_machine_list.add(machine_list.get(j));
          machine_list.remove(j);
          j--; //so that next loop doesnt skip a machine
        }
      }
    }
    for(int j = 0; j < machine_list.size(); j++)
    {
      closed_machine_list.add(machine_list.get(j));
    }

    return closed_machine_list;
  }

  public static void main(String[] args)
  {
    File dataFile = null;
    //get file from comman line
    if(0 < args.length )
    {
      dataFile = new File(args[0]);

      ArrayList<Service> service_list = new ArrayList<Service>();
      ArrayList<Machine> machine_list = new ArrayList<Machine>();
      //new object for NF
      NF data = new NF();

      service_list = data.ReadFile(dataFile);
      machine_list = data.create_machine_list();
      long start = System.currentTimeMillis();

      machine_list = data.next_fit(service_list,machine_list);
      long end = System.currentTimeMillis();

      //print out the result of the experiment
      int solution = data.acc_solution(machine_list);
      System.out.println(end-start+"ms");
      System.out.println("LB solution: " + data.optimal_solution(service_list));
      System.out.println("actual solution: " + solution);
      System.out.println("objective function in % (actual - LB/LB)*100 : " + (solution-data.optimal_solution(service_list))/(double)data.optimal_solution(service_list) * 100);

    }
    else
    {
      System.err.println("Invalid arguments count:" + args.length);
      System.exit(0);
    }
  }

  /*
  //BUlK run for the EXPERIMENT in each data set sizes
    public static void main(String[] args)
    {
      //variable for data
      long[] time_list = new long[100];
      int[] op_list = new int[100];
      int[] acc_list = new int[100];
      long avg_time = 0;

      File dataFile = null;
      //get file from command line
      if(0 < args.length && !args[0].equals("full"))
      {
        int[] list_op = new int[100];
        int avg_op = 0;

        int[] list_acc = new int[100];
        double avg_acc = 0;
        System.out.println("dataset_"+args[0]+": ");
        for(int i = 0; i < 100 ; i++)
        {
          dataFile = new File("/Users/ed_garden/Documents/Individual_projects/heuristic/dataset1/apps_"+args[0]+"/dataset_"+args[0]+"_"+i+".csv");
          //new object for NF
          NF data = new NF();

          ArrayList<Service> service_list = new ArrayList<Service>();
          ArrayList<Machine> machine_list = new ArrayList<Machine>();

          service_list = data.ReadFile(dataFile);
          machine_list = data.create_machine_list();

          long start = System.currentTimeMillis();
          //service_list = data.count_sort(service_list);
          machine_list = data.next_fit(service_list,machine_list);
          long end = System.currentTimeMillis();

          // analaysis
          time_list[i] = end-start;
          op_list[i] = data.optimal_solution(service_list);
          acc_list[i] = data.acc_solution(machine_list);
        }

        //print out analaysis and avg analaysis
        NF data = new NF();

        System.out.println("Duration: "+Arrays.toString(time_list));
        System.out.println("LB Executor: "+Arrays.toString(op_list));
        System.out.println("Actual Executor: "+Arrays.toString(acc_list));

        for(int i = 0; i < 100; i++)
        {
          avg_time += time_list[i];
        }
        System.out.println("Average time taken is: "+ avg_time/100);
        double[] grade = data.grade_heuristic(op_list,acc_list);
        System.out.println("Accuracy in % (actual - LB/LB)*100 : " + Arrays.toString(grade));
        System.out.println("Final Accuracy(%): "+ data.avg_grade(grade)+ "%");
      }
      else
      {
        System.err.println("Invalid arguments count:" + args.length);
        System.exit(0);
      }
    }
  */
}
