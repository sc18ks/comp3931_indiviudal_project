package Bin_Packing;


import java.io.*;
import java.util.stream.Collectors;
import java.util.Scanner;
import java.util.ArrayList;
import java.lang.*;
import java.util.PriorityQueue;
import java.util.Comparator;
import java.util.Collections;
import java.util.Arrays;


public class Heuristic
{
  //read file from commnad line
  public static ArrayList<Service> ReadFile(File dataFile)
  {
    BufferedReader br = null;
    ArrayList<Service> service_list = new ArrayList<Service>();
    try
    {
      String data = "";
      //read to BufferedReaders
      br = new BufferedReader(new FileReader(dataFile));
      data =  br.lines().collect(Collectors.joining("\n"));
      service_list = data_sort(data);
    }
    catch (FileNotFoundException ex){ex.printStackTrace();}
    finally
    {
        if (br != null)
        {
            try
            {
                br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    return service_list;
   }

  public static ArrayList<Service> data_sort(String file)
  {

    String data = "";
    Scanner line = new Scanner(file);

    ArrayList<Service> service_list = new ArrayList<Service>();
    ArrayList<Integer> instances = new ArrayList<Integer>();

    //filling service list
    //read line by line except first line
    data = line.nextLine();

    while (line.hasNextLine())
    {
      data = line.nextLine();

      //spliting data in parts
      String[] parts = data.trim().split("\t");

      int service_id = Integer.parseInt(parts[0]);
      int nb_instances = Integer.parseInt(parts[1]);
      int core = Integer.parseInt(parts[2]);

      //if the core value is more than 92 skip it
      if(core <= 92)
      {
        //puting data in a arraylist of service object
        Service new_serivce = new Service(service_id, core, parts[7]);
        service_list.add(new_serivce);

        //get the number of intances of each service
        instances.add(nb_instances);
      }
     }

    //get all the nb_instances form each service
    int list_size = service_list.size();

    for(int i = 0; i < list_size;i++)
    {
      for(int j =0; j < instances.get(i)-1;j++)  //already have one initially
      {
        service_list.add(service_list.get(i));
      }
    }
    return service_list;
  }

  //Artifical instance of the executor size 22000 for LIST
  //Use for real experiment
  public static ArrayList<Machine> create_machine_list()
  {
    ArrayList<Machine> machine_list = new ArrayList<Machine>();
    int machine_number = 50000;
    int core_value = 92;
    //set_1
    for(int i = 0; i < machine_number; i++)
    {
      Machine new_machine = new Machine(i+1, core_value); //machine_id start from 1
      machine_list.add(new_machine);
    }
    return machine_list;
  }


  //Artifical instance of the executor size 22000 for BF
  //Use for real experiment
  public static PriorityQueue<Machine> create_machine()
  {
    PriorityQueue<Machine> machine_list = new PriorityQueue<Machine>();
    int machine_number = 50000;
    int core_value = 92;

    //creat each machine
    for(int i = 0; i < machine_number; i++)
    {
      Machine new_machine = new Machine(i+1, core_value); //machine_id start from 1
      machine_list.add(new_machine);
    }
    return machine_list;
  }

  //Artifical instance of the executor size 22000 for WF
  //Use for real experiment
  public static PriorityQueue<Machine> create_machine_WF(PriorityQueue<Machine> machine_list)
  {
    int machine_number = 50000;
    int core_value = 92;
    //set_1
    for(int i = 0; i < machine_number; i++)
    {
      Machine new_machine = new Machine(i+1, core_value); //machine_id start from 1
      machine_list.add(new_machine);
    }
    return machine_list;
  }


  //Print value in service LIST
  public static int print_serivce(ArrayList<Service> service_list)
  {
    int sum = 0;
    for (int i = 0; i < service_list.size(); i++)
    {
      System.out.print(service_list.get(i).service_id+ " " +service_list.get(i).core + " ");
      System.out.println(service_list.get(i).aff_list);
      sum += service_list.get(i).core;
    }
    return sum;
  }

  //Print machine for LIST
  public static void print_machine_list(ArrayList<Machine> machine_list)
  {
    System.out.println(machine_list);
  }

  //Print machine for PQ
  public static void print_machine(PriorityQueue<Machine> machine_list)
  {
    System.out.println(machine_list);
  }

  public static ArrayList<Service> count_sort(ArrayList<Service> l)
  {
    int max_core_value = 92;
    ArrayList<Integer> temp = new ArrayList<Integer>();
    ArrayList<Service> sorted_l = new ArrayList<Service>();
    //Add as many elements as there is in l to sorted_l
    for(int i=0; i<l.size()+1; i++)
    {
      sorted_l.add(new Service(0,0,"[]"));
    }
    //Step 1 initialize
    for(int i=0; i<max_core_value; i++)
    {
      temp.add(0);
    }
    //Step 2
    for(int i=0; i<l.size(); i++)
    {
      temp.set(l.get(i).core, temp.get(l.get(i).core) + 1);
    }
    //Step 3
    for(int i=0; i<temp.size()-1; i++)
    {
      temp.set(i+1, temp.get(i+1) + temp.get(i));
    }
    //Step 4
    for(int i=0; i<l.size(); i++)
    {
      sorted_l.set(temp.get(l.get(i).core), l.get(i));
      temp.set(l.get(i).core, temp.get(l.get(i).core) -1);
    }
    //Step 5 in decreasing order
    for(int i=0; i<Math.floor(sorted_l.size()/2); i++)
    {
      Service t = sorted_l.get(i);
      sorted_l.set(i, sorted_l.get(sorted_l.size()-i-1));
      sorted_l.set(sorted_l.size()-i-1,t);
    }
    sorted_l.remove(sorted_l.size() -1); //remove the first index
    return sorted_l;
  }

//EXPERIMENT AND ANALAYSIS

  //Return the optimal solution
  public static int optimal_solution(ArrayList<Service> service_list)
  {
    //optimal machine for service's core
    double sumServiceCore = 0;
    double optimalMachine = 0;

    //count the number of service's core
    for (int i = 0; i < service_list.size(); i++)
    {
      sumServiceCore += service_list.get(i).core;
    }

    //Artifical instance where all core value of the executor is 92
    optimalMachine = sumServiceCore/92;

    int op_mac = (int)Math.ceil(optimalMachine);
    return op_mac;
  }

  //For BF and WF

  //return the number of machine that the solution need
  //this will get rid of all the machine in the list
  public static int heuristic_solution(PriorityQueue<Machine> machine_list)
  {
    int size = machine_list.size();
    int machine_num = 0;
    for (int i = 0; i < size; i++)
    {
      if(!machine_list.poll().service_list.isEmpty())
      {
        machine_num = machine_num + 1;
      }
    }
    return machine_num;
  }

  //For FF and NF
  public static int acc_solution(ArrayList<Machine> machine_list)
  {
    int machine_num = 0;
    for (int i = 0; i < machine_list.size(); i++)
    {
      if(!machine_list.get(i).service_list.isEmpty())
      {
        machine_num += 1;
      }
    }
    return machine_num;
  }

  //The OBJECTIVE FUNCTION

  //For calculating objective function
  public static double[] grade_heuristic(int[] op_mac, int[] acc_mac)
  {
    double[] grade = new double [100];
    for(int i = 0; i < 100; i++)
    {
      grade[i] = ((acc_mac[i] - op_mac[i])/(double)op_mac[i]) *100; //accuracy in percent
    }
    return grade;
  }

  //For AVG objective function
  public static double avg_grade(double[] grade)
  {
    double sum_grade = 0;
    for(int i = 0; i < grade.length; i++)
    {
      sum_grade += grade[i];
    }
    return sum_grade/grade.length;
  }

//TESTING
  //create executor for testing small LIST
  public static ArrayList<Machine> create_machine_test_List_S()
  {
    ArrayList<Machine> machine_list = new ArrayList<Machine>();
    int machine_number = 5;
    int core_value = 92;

    //set_1
    for(int i = 0; i < machine_number; i++)
    {
      Machine new_machine = new Machine(i+1, core_value); //machine_id start from 1
      machine_list.add(new_machine);
    }
    return machine_list;
  }

  //create executor for testing large LIST
  public static ArrayList<Machine> create_machine_test_List_L()
  {
    ArrayList<Machine> machine_list = new ArrayList<Machine>();
    int machine_number = 13000;
    int core_value = 92;

    //set_1
    for(int i = 0; i < machine_number; i++)
    {
      Machine new_machine = new Machine(i+1, core_value); //machine_id start from 1
      machine_list.add(new_machine);
    }
    return machine_list;
  }

  //create executor for testing small PQ BF
  public static PriorityQueue<Machine> create_machine_test_S()
  {
    PriorityQueue<Machine> machine_list = new PriorityQueue<Machine>();
    int machine_number = 5;
    int core_value = 92;

    //set_1
    for(int i = 0; i < machine_number; i++)
    {
      Machine new_machine = new Machine(i+1, core_value); //machine_id start from 1
      machine_list.add(new_machine);
    }
    return machine_list;
  }

  //create executor for testing large PQ BF
  public static PriorityQueue<Machine> create_machine_test_L()
  {
    PriorityQueue<Machine> machine_list = new PriorityQueue<Machine>();
    int machine_number = 13000;
    int core_value = 92;

    //set_1
    for(int i = 0; i < machine_number; i++)
    {
      Machine new_machine = new Machine(i+1, core_value); //machine_id start from 1
      machine_list.add(new_machine);
    }
    return machine_list;
  }

  //For WF

  //create executor for testing small WF
  //because it wont sort if we didnt not pass the PQ as a parameter
  public static PriorityQueue<Machine> create_machine_WF_S(PriorityQueue<Machine> machine_list)
  {
    int machine_number = 5;
    int core_value = 92;

    //set_1
    for(int i = 0; i < machine_number; i++)
    {
      Machine new_machine = new Machine(i+1, core_value); //machine_id start from 1
      machine_list.add(new_machine);
    }
    return machine_list;
  }

  //create executor for testing large WF
  public static PriorityQueue<Machine> create_machine_WF_L(PriorityQueue<Machine> machine_list)
  {
    int machine_number = 13000;
    int core_value = 92;

    //set_1
    for(int i = 0; i < machine_number; i++)
    {
      Machine new_machine = new Machine(i+1, core_value); //machine_id start from 1
      machine_list.add(new_machine);
    }
    return machine_list;
  }

}
